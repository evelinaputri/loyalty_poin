document.addEventListener("click", handle);

function handle(evt) {
   if (evt.target.type === "button") {
    return handleBtn(evt.target);
  }
}

function handleBtn(btn) {
  const elem = document.querySelector(`#${btn.dataset.for}`);
  const nwValue = +elem.value + (btn.value === "-" ? -1 : 1);
  elem.value = nwValue >= +elem.min ? nwValue : elem.min;
}


function myFunctionmenu() {
  var x = document.getElementById("menu_inner");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}


function myFunction() {
  var element = document.getElementById("sub");
  element.classList.add("show");
}
function myFunction2() {
  var element = document.getElementById("sub2");
  element.classList.add("show");
}
function myFunction3() {
  var element = document.getElementById("sub3");
  element.classList.add("show");
}
function myFunction4() {
  var element = document.getElementById("");
  element.classList.add("show");
}

